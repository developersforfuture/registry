# Registry

To handle several web projects we do need some kind of docker registry and a place to build them. As Gitlab has that nice feature, we will use it and share the images across all other projects.